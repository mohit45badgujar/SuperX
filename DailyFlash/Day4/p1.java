/*
	 A B C D
	 # # # #
	 A B C D
	 # # # #
	 A B C D

*/
import java.util.*;
class pattern {
	public static void main(String[] mohit){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		
		for(int i=1;i<=row;i++){
			char ch='A';
			for(int j=1;j<=row-1;j++){
				if(i%2==1){
					System.out.print(ch+" ");
					ch++;
				}else{
					System.out.print("# ");
				}
			}
			System.out.println();
		}
	}
}
