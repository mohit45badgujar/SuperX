
// WAP to print the odd number in the given range
// input : start : 1
//         end :10


import java.util.*;
class oddInRange {
	public static void main(String[] mohit){
		Scanner sc=new Scanner(System.in);
		int start=sc.nextInt();
		int end=sc.nextInt();

		for(int i=start;i<=end;i++){
			if(i%2 != 0)
				System.out.println(i);
		}
	}
}
